class User
  include Mongoid::Document
  include Mongoid::Timestamps
  field :firstName, type: String
  field :lastName, type: String
  field :email, type: String

  def self.search(input)
    User.or({firstName: /#{input}/i}, {lastName: /#{input}/i}, {email: /#{input}/i})
  end
end
