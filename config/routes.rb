Rails.application.routes.draw do
  scope :api do
   get 'users', to: 'users#index'
   get  'users/:id', to: 'users#show'
   post 'user', to: 'users#create'
   delete 'user/:id', to: 'users#destroy'
   put 'user/:id', to: 'users#update'
   get 'typeahead/:input', to: 'users#search'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
